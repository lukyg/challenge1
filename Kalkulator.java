import java.util.Scanner;

public class Kalkulator {
    public static void main (String[]args){
        int pilihan;
        boolean kembali1 = true;
        do{
            System.out.println(menu_utama("-------------------------------------"));
            System.out.print("Masukkan pilihan = ");
            Scanner input_pilihan = new Scanner (System.in);
            pilihan = input_pilihan.nextInt();
            if(pilihan == 1){
                boolean kembali2 = true;
                int pilihan_luas;
                do {
                    System.out.println(menu_luas("-------------------------------------"));
                    System.out.print("Masukkan pilihan = ");
                    Scanner input_pilihan_luas = new Scanner(System.in);
                    pilihan_luas = input_pilihan_luas.nextInt();
                    switch (pilihan_luas) {
                        case 1:
                            System.out.println(persegi(0));
                            System.out.println("\n");
                            break;
                        case 2:
                            System.out.println(persegi_panjang(0, 0));
                            System.out.println("\n");
                            break;
                        case 3:
                            System.out.println(segitiga(0, 0));
                            System.out.println("\n");
                            break;
                        case 4:
                            System.out.println(lingkaran(0));
                            System.out.println("\n");
                            break;
                        case 0:
                            kembali2 = false;
                            break;
                        default:
                            System.out.println("Masukkan pilihan yang benar bestie :*");
                            break;
                    }
                }while(kembali2);
            }
            else if (pilihan == 2){
                boolean kembali3 = true;
                int pilihan_volume;
                do{
                    System.out.println(menu_volume("-------------------------------------"));
                    System.out.print("Masukkan pilihan = ");
                    Scanner input_pilihan_volume = new Scanner (System.in);
                    pilihan_volume = input_pilihan_volume.nextInt();
                    switch (pilihan_volume) {
                        case 1:
                            System.out.println(kubus(0));
                            System.out.println("\n");
                            break;
                        case 2:
                            System.out.println(balok(0, 0, 0));
                            System.out.println("\n");
                            break;
                        case 3:
                            System.out.println(tabung(0, 0));
                            System.out.println("\n");
                            break;
                        case 0:
                            kembali3 = false;
                            break;
                        default:
                            System.out.println("Masukkan pilihan yang benar sis :v");
                            break;
                    }
                }while(kembali3);
            }
            else if (pilihan == 0){
                System.out.println("-------------------------------------");
                System.out.println("             Terimakasih <3          ");
                System.out.println("-------------------------------------");
                System.exit(0);
            }
            else{
                System.out.println("Masukkan pilihan yang benar gann :D");
            }
        }while (kembali1);
    }

    public static int persegi(int sisi_persegi){
        System.out.println("-------------------------------------");
        System.out.println("        Penghitung Luas Persegi      ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan sisi = ");
        Scanner input_persegi = new Scanner (System.in);
        sisi_persegi = input_persegi.nextInt();
        System.out.print("\n\nWooshh... hasilnya adalah ");
        return sisi_persegi*sisi_persegi;
    }

    public static int persegi_panjang(int panjang_persegi, int lebar_persegi){
        System.out.println("-------------------------------------");
        System.out.println("   Penghitung Luas Persegi Panjang   ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan panjang = ");
        Scanner input_panjang_persegi = new Scanner (System.in);
        panjang_persegi = input_panjang_persegi.nextInt();
        System.out.print("Masukkan lebar = ");
        Scanner input_lebar_persegi = new Scanner (System.in);
        lebar_persegi = input_lebar_persegi.nextInt();
        System.out.print("\n\nTwwiinggg... hasilnya adalah ");
        return panjang_persegi*lebar_persegi;
    }

    public static double segitiga(double alas_segitiga, double tinggi_segitiga){
        System.out.println("-------------------------------------");
        System.out.println("       Penghitung Luas Segitiga      ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan alas = ");
        Scanner input_alas_segitiga = new Scanner (System.in);
        alas_segitiga = input_alas_segitiga.nextDouble();
        System.out.print("Masukkan tinggi = ");
        Scanner input_tinggi_segitiga = new Scanner (System.in);
        tinggi_segitiga = input_tinggi_segitiga.nextDouble();
        System.out.print("\n\nTadaaaa... hasilnya adalah ");
        return 0.5 * alas_segitiga * tinggi_segitiga;
    }

    public static double lingkaran(double jari_lingkaran){
        System.out.println("-------------------------------------");
        System.out.println("       Penghitung Luas Lingkaran     ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan jari-jari = ");
        Scanner input_jari_lingkaran = new Scanner (System.in);
        jari_lingkaran = input_jari_lingkaran.nextDouble();
        System.out.print("\n\nTaraaaa... hasilnya adalah ");
        return 3.14 * jari_lingkaran * jari_lingkaran;
    }

    public static int kubus(int sisi_kubus){
        System.out.println("-------------------------------------");
        System.out.println("       Penghitung Volume Kubus       ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan sisi = ");
        Scanner input_sisi_kubus = new Scanner (System.in);
        sisi_kubus = input_sisi_kubus.nextInt();
        System.out.print("\n\nWoooshh... hasilnya adalah ");
        return sisi_kubus * sisi_kubus * sisi_kubus;
    }

    public static int balok(int panjang_balok, int lebar_balok, int tinggi_balok){
        System.out.println("-------------------------------------");
        System.out.println("       Penghitung Volume Balok       ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan panjang = ");
        Scanner input_panjang_balok = new Scanner (System.in);
        panjang_balok = input_panjang_balok.nextInt();
        System.out.print("Masukkan lebar = ");
        Scanner input_lebar_balok = new Scanner (System.in);
        lebar_balok = input_lebar_balok.nextInt();
        System.out.print("Masukkan tinggi = ");
        Scanner input_tinggi_balok = new Scanner (System.in);
        tinggi_balok = input_tinggi_balok.nextInt();
        System.out.print("\n\nSatsetsatset... hasilnya adalah ");
        return panjang_balok * lebar_balok * tinggi_balok;
    }

    public static double tabung (double jari_tabung, double tinggi_tabung){
        System.out.println("-------------------------------------");
        System.out.println("       Penghitung Volume Tabung      ");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan jari-jari = ");
        Scanner input_jari_tabung = new Scanner (System.in);
        jari_tabung = input_jari_tabung.nextDouble();
        System.out.print("Masukkan tinggi = ");
        Scanner input_tinggi_tabung = new Scanner (System.in);
        tinggi_tabung = input_tinggi_tabung.nextDouble();
        System.out.print("\n\nDann... hasilnya adalah ");
        return 3.14 * jari_tabung * jari_tabung * tinggi_tabung;
    }

    public static String menu_utama(String menu_u){
        System.out.println("-------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("-------------------------------------");
        System.out.println("1. Menghitung Luas");
        System.out.println("2. Menghitung Volume");
        System.out.println("0. Keluar");
        return menu_u;
    }

    public static String menu_luas(String menu_l){
        System.out.println("-------------------------------------");
        System.out.println("     Penghitung Luas Bangun Datar    ");
        System.out.println("-------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi panjang");
        System.out.println("3. Segitiga");
        System.out.println("4. Lingkaran");
        System.out.println("0. Kembali");
        return menu_l;
    }

    public static String menu_volume(String menu_v){
        System.out.println("-------------------------------------");
        System.out.println("    Penghitung Volume Bangun Ruang   ");
        System.out.println("-------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali");
        return menu_v;
    }
}